import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CustomPluginComponent } from './components/custom-plugin/custom-plugin.component';
import { CheckoutBootstrapComponent } from './components/checkout-bootstrap/checkout-bootstrap.component';
import { CheckoutMaterializeComponent } from './components/checkout-materialize/checkout-materialize.component';
import { CustomPlugin2Component } from './components/custom-plugin2/custom-plugin2.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CheckoutComponent,
    CarouselComponent,
    CustomPluginComponent,
    CheckoutBootstrapComponent,
    CheckoutMaterializeComponent,
    CustomPlugin2Component,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
