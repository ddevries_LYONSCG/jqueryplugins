import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'lyons-query-checkout-bootstrap',
  templateUrl: './checkout-bootstrap.component.html',
  styleUrls: ['./checkout-bootstrap.component.css']
})
export class CheckoutBootstrapComponent implements OnInit {

  constructor() {}

  ngOnInit() {

    $('#phone').on('keyup', function (e) {
      const re = new RegExp('-', 'g');
      const input = $(this).val().toString().replace(re, '');

      let formatted = '';
      for (let i = 0; i < input.length; ++i) {
        if (i === 3) {
          formatted += '-';
        } else if (i === 6) {
          formatted += '-';
        }
        formatted += input.charAt(i);
      }
      $(this).val(formatted);
    });

    $.validator.setDefaults({
      errorClass: 'error-text',
      highlight: function (element) {
        $(element).addClass('is-invalid');
        $(element).removeClass('is-valid');
      },
      unhighlight: function (element) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');

      },
      errorPlacement: function (error, element) {
        if (element.prop('id') === 'address1') {
          console.log(element);
          error.insertAfter(element.parent().find('#address2'));
        } else {
          error.insertAfter(element);
        }
      }
    });

    $.validator.addMethod('myPhone', (value, element) => {
      return (value === '2693333333');
    }, 'Please enter my phone number!');

    $('#checkout-form').validate({
      rules: {
        firstName: {
          required: true
        },
        lastName: {
          required: true
        },
        address: {
          required: true
        },
        zip: {
          required: true,
          zipcodeUS: true
        },
        phone: {
          required: true,
          phoneUS: true
        },
        card: {
          required: true,
          creditcard: true

        }
      },
      messages: {
        firstName: {
          required: 'Please enter a first name.'
        },
        lastName: {
          required: 'Please enter a last name.'
        },
        address: {
          required: 'Please enter a valid address'
        },
        zip: {
          required: 'Please enter a valid zip code'
        }
      }
    });
  }
}
