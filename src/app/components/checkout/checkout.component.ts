import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'lyons-query-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  constructor() {}

  ngOnInit() {
    $.validator.setDefaults({
      errorClass: 'error-text',
      highlight: function (element) {
        $(element).addClass('has-error');
      },
      unhighlight: function (element) {
        $(element).removeClass('has-error');
      }
      // errorPlacement: function(error, element) {
      //   if (element.prop('type') === 'thisIsAnExample') {
      //     error.insertAfter(element.parent());
      //   } else {
      //     error.insertAfter(element);
      //   }
      // }
    });

    $('#phone').on('keyup', function(e) {
      const re = new RegExp('-', 'g');
      const input = $(this).val().toString().replace(re, '');

      let formatted = '';
      for (let i = 0; i < input.length; ++i) {
        if (i === 3) {
          formatted += '-';
        } else if (i === 6) {
          formatted += '-';
        }
        formatted += input.charAt(i);
      }
      $(this).val(formatted);
    });

    $.validator.addMethod('myPhone', (value, element) => {
      return (value === '2697444109');
    }, 'Please enter my phone number!');

    $('#checkout-form').validate({
      rules: {
        firstName: {
          required: true
        },
        lastName: {
          required: true
        },
        address: {
          required: true
        },
        zip: {
          required: true,
          zipCodeUS: true
        },
        phone: {
          required: true,
          phoneUS: true
        },
        card: {
          required: true,
          creditcard: true

        }
      },
      messages: {
        firstName: {
          required: 'Please enter a first name.'
        },
        lastName: {
          required: 'Please enter a last name.'
        },
        address: {
          required: 'Please enter a valid address'
        },
        zip: {
          required: 'Please enter a valid zip code'
        }
      }
    });
  }

}
