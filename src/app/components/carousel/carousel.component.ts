import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'lyons-query-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  constructor() {}

  ngOnInit() {

    $('#carousel-1').slick({
      autoplay: true,
      autoplaySpeed: 2000,
      speed: 500,
      fade: true,
    });

    $('#carousel-2').slick({
      autoplay: false,
      speed: 1000,
      dots: true,
      cssEase: 'ease',
      swipe: true,
    });

    $('#carousel-3').slick({
      autoplay: true,
      autoplaySpeed: 0,
      speed: 3000,
      vertical: true,
    });
  }

}
