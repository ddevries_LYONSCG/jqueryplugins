import { Component, OnInit, AfterViewInit } from '@angular/core';

$.fn.ghost = function() {
  return {
    initialize: function() {

    },
    ghostify: function(targets, options = {}) {
      const defaults = {
        intervalTime: 2000,
        offsetTime: 0,
        scaleDown: 0.85,
        scaleUp: 1.15
      };
      const settings: any = Object.assign({}, defaults, options);
      settings.shadowBase = '0px 0px 20px 5px rgba(200, 200, 200, 0.95)';
      settings.shadowDown = '0px 0px 15px 4px rgba(165,165,165,0.95)';
      settings.shadowUp = '0px 0px 50px 10px rgba(215,215,215,0.75)';
      settings.offset = 0;

      $.each(targets, (index, value) => {
        const ghostEl = $(value);
        settings.offset += settings.offsetTime;
        ghostEl.css({
          // tslint:disable-next-line:max-line-length
          'transition': `transform ${settings.intervalTime}ms ease-in-out ${settings.offset}ms,box-shadow ${settings.intervalTime}ms ease-in-out ${settings.offset}ms`,
          'box-shadow': `${settings.shadowBase}`,
        });

        let hover = false;
        let scaleDown = true;
        setInterval(function() {
          if (!hover) {
            if (scaleDown) {
              ghostEl.css({'transform': `scale(${settings.scaleDown})`});
              ghostEl.css({'box-shadow': `${settings.shadowDown}`});
              scaleDown = false;
            } else {
              ghostEl.css({'transform': `scale(${settings.scaleUp})`});
              ghostEl.css({'box-shadow': `${settings.shadowUp}`});
              scaleDown = true;
            }
          }
        }, (settings.intervalTime + settings.offset));

        ghostEl.on({
          mouseenter: function () {
            hover = true;
            ghostEl.css({
              'transition': `transform 0.5s ease-in-out,
                             box-shadow 0.5s ease-in-out`,
              'transform': 'scale(1)',
              'box-shadow': `${settings.shadowBase}`,
            });
          },
          mouseleave: function () {
            hover = false;
            ghostEl.css({
              // tslint:disable-next-line:max-line-length
              'transition': `transform ${settings.intervalTime}ms ease-in-out ${settings.offset}ms, box-shadow ${settings.intervalTime}ms ease-in-out ${settings.offset}ms`
            });
          }
        });
      });
    }
  };
};

@Component({
  selector: 'lyons-query-custom-plugin2',
  templateUrl: './custom-plugin2.component.html',
  styleUrls: ['./custom-plugin2.component.css']
})
export class CustomPlugin2Component implements AfterViewInit {
  normalRows: any[];
  normalCols: any[];
  offsetRows: any[];
  offsetCols: any[];

  constructor() {
    this.normalRows = Array.from(Array(4));
    this.normalCols = Array.from(Array(8));
    this.offsetRows = Array.from(Array(6));
    this.offsetCols = Array.from(Array(8));
  }
  ngAfterViewInit() {
    const Ghost = $.fn.ghost();
    Ghost.initialize();
    Ghost.ghostify($('.ghost-large'));
    Ghost.ghostify($('.ghost'));
    Ghost.ghostify($('.ghost-offset'), {
      intervalTime: 1500,
      offsetTime: 10,
      scaleDown: 0.85,
      scaleUp: 1.15
    });
    Ghost.ghostify($('#ghost-button'), {
      intervalTime: 1500,
      scaleDown: 0.96,
      scaleUp: 1.04
    });
  }
}
