import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lyons-query-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title: string;
  constructor() {
    this.title = 'jQuery Plugins';
   }

  ngOnInit() {

  }

}
