
  // motion blur on websites; creation of child divs that have lower opacity and move behind on animation; this can be a feature
  // all margins effect the calculation of centerness, translation, etc.
  /*
    the plugin will give the option to make a certain element an attention seeker
      args:
        - need to specify an area of awareness for the attention seeker
        - need to specify an area of to which it can move to recieve attention; define boundaries
        - how quickly it seeks the user (animation easing)
      issues:
        - define mouseover areas; this should be aligned with the first arg

    STEP
    given a target element create a new element that defines a region around it (by the given parameters)
    assign an event handler
  */

import {
  Component,
  OnInit
} from '@angular/core';
import {
  element
} from 'protractor';
import {
  resolve
} from 'q';
import {
  parse
} from 'querystring';

/* ATTENTION SEEKER */
$.fn.seeker = function () {
  let count: number;
  let verbose: boolean;

  return {
    initalize: (MODE = undefined) => {
      count = 0;
      verbose = (MODE === 'DEBUG') ? true : false;
    },
    seek: (targets, options = <any>{}) => {
      options = {
        aoiWidth : 20,
        aoiHeight : 20,
        inheritDimensions : true,
        aomWidth : 50,
        aomHeight : 50,
        speed: 1
      };

      $.each(targets, (index, value) => {
        const seekerEl = $(value);
        const aoiEl = _createAoiEl(seekerEl);
        seekerEl.css({
          'transition': 'transform 1s linear'
        });
        aoiEl.css({
          'transition': 'transform 1s linear'
        });
        aoiEl.on('mousemove', (cursor) => {
          const x = cursor.pageX - aoiEl.offset().left - aoiEl.width() / 2;
          const y = cursor.pageY - aoiEl.offset().top - aoiEl.height() / 2;
          aoiEl.css({
            'transform': `translateX(${-(x) - (options.aoiWidth / 2)}px) translateY(${-(y) - (options.aoiHeight / 2)}px)`
          });
          seekerEl.css({
            'transform': `translateX(${x}px) translateY(${y}px)`
          });
        });
        aoiEl.on({
          mouseenter: function () {
            console.log('ENTER');
          },
          mouseleave: function () {
            console.log('EXIT');
            seekerEl.css({
              'transform': 'translateX(0) translateY(0)'
            });
            aoiEl.css({
              'transform' : `translateX(${(-options.aoiWidth / 2)}px) translateY(${(-options.aoiHeight / 2)}px)`
            });
          }
        });
      });

      function _createAoiEl(target) {
        const aoiEl = $(`<span id='seeker-${count++}'></span>`);
        let width = options.aoiWidth;
        let height = options.aoiHeight;
        if (options.inheritDimensions) {
          const borderX = (parseFloat(target.css('borderRightWidth'))) + (parseFloat(target.css('borderLeftWidth')));
          const borderY = (parseFloat(target.css('borderTopWidth'))) + (parseFloat(target.css('borderBottomWidth')));
          width += target.innerWidth() + borderX;
          height += target.innerHeight() + borderY;
        }
        aoiEl.css({
          'width': width,
          'height': height,
          'position': 'absolute',
          'top': 0,
          'left': 0,
          'transform' : `translateX(${(-options.aoiWidth / 2)}px) translateY(${(-options.aoiHeight / 2)}px)`,
          'border': (verbose) ? '1px solid red' : 'none'
        });
        aoiEl.appendTo(target);
        return aoiEl;
      }
    },
  };
};

@Component({
  selector: 'lyons-query-custom-plugin',
  templateUrl: './custom-plugin.component.html',
  styleUrls: ['./custom-plugin.component.css']
})
export class CustomPluginComponent implements OnInit {

  constructor() {}

  ngOnInit() {

    const AttentionSeeker = $.fn.seeker();
    AttentionSeeker.initalize('DEBUG');
    AttentionSeeker.seek($('.list-group-item'));
    AttentionSeeker.seek($('.seeker'));
  }
}
